### Récupération de projet
- git clone https://gitlab.com/spurse/sf60-tasktagformembeded.git

### Installation
- Dans le dossier sf60-tasktagformembeded faire :
- composer install
- dans le fichier .env configurer la base de données
- symfony console d:d:c
- symfony console d:s:u --force

### Lancer le site
- symfony serve