<?php

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TaskRepository::class)]
class Task
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'task', targetEntity: Tag::class, cascade: ['persist'], orphanRemoval:true)]
    // orphanRemoval=true => Quand on fait un edit d'une task et qu'on supprime un tag, le tag devient orphelin donc il faut le supprimer
    private Collection $tags;


    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->setTask($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        // set the owning side to null (unless already changed)
        if ($this->tags->removeElement($tag) && $tag->getTask() === $this) {
            $tag->setTask(null);
        }

        return $this;
    }
}
